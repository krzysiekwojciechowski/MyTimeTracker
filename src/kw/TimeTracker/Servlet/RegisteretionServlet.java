package kw.TimeTracker.Servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/register.do")
public class RegisteretionServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userLogin = req.getParameter("userLogin");
        String password1 = req.getParameter("password1");
        String password2 = req.getParameter("password2");
        String firstName = req.getParameter("firstName");
        String secName = req.getParameter("secName");
        String birthyear = req.getParameter("birthyear");

        /**
         * walidacja danych nowego usera
         * loginUser unikalny
         * pass1 i pass2 takie same
         * data urodzenia int
         */

        if (isUsersDataValid(userLogin,password1,password2,firstName,secName,birthyear)) {
            resp.sendRedirect(resp.encodeRedirectURL("usersData.jsp"));
            HttpSession session = req. getSession();
            session.setAttribute("userLogin", userLogin);
            session.setAttribute("firstName", firstName);
            session.setAttribute("secName", secName);
        } else {

        }
    }

    private boolean isUsersDataValid(String userLogin, String pass1, String pass2, String fName, String secName, String bYear) {
        if (!pass1.equals(pass2)) {
            return false;
        }
        if (userLogin == null || pass1 == null || pass2 == null || fName == null || secName == null)
        return false;

        if (isBirthYearValid(bYear)){
            return false;
        }
        return true;
    }

    private boolean isBirthYearValid(String birthYear) {
        try {
            Integer.parseInt(birthYear);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}

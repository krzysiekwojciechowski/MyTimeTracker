package kw.TimeTracker.Servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login.do")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userLogin = req.getParameter("userLogin");
        String myPassword = req.getParameter("myPassword");

        if ("admin".equals(userLogin) && "admin".equals(myPassword)) {
            req.getSession().setAttribute("userLogin", userLogin);
            resp.sendRedirect(resp.encodeRedirectURL("homePage.jsp"));
        } else {
            resp.sendRedirect(resp.encodeRedirectURL("login.jsp"));
        }
    }
}

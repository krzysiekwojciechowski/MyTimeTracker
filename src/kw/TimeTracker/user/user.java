package kw.TimeTracker.user;

public class user {
    private String userLogin;
    private String name;
    private String secName;
    private String password;
    private int birthYear;
    private int id;

    public user(String userLogin, String name, String secName, String password, int birthYear, int id) {
        this.userLogin = userLogin;
        this.name = name;
        this.secName = secName;
        this.password = password;
        this.birthYear = birthYear;
        this.id = id;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecName() {
        return secName;
    }

    public void setSecName(String secName) {
        this.secName = secName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Time Tracker</title>
    <link rel="stylesheet" type="text/css" href="styles.css?version=1">
</head>
<body class="body">
<div class="content">
   My data:
    <ul>
        <li>
            UserLogin: <%=session.getAttribute("userLogin")%>
        </li>
        <li>
            First name: <%=session.getAttribute("firstName")%>
        </li>
        <li>
            Second name: <%=session.getAttribute("secName")%>
        </li>
    </ul>
</div>

<%@include file="header.jsp" %>
<%@include file="menu.jsp" %>
<%@include file="footer.jsp" %>
</body>
</html>

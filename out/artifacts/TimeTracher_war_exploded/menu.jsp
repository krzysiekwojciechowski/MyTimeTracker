<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Time Tracker</title>
    <link rel="stylesheet" type="text/css" href="styles.css?version=1">
</head>
<body>
<div class="menu">
    <ul>
        <li>
            <a href="contact.jsp">Kontakt</a>
        </li>
        <li>
            <a href="about.jsp">O nas</a>
        </li>
        <li>
            <a href="usersList.jsp">Lista użytkowników (JSP)</a>
        </li>
        <li>
            <a href="usersListServlet.jsp">Lista użytkowników (Servlet)</a>
        </li>
        <li>
            <a href="workTime.jsp">Zgłoś czas pracy</a>
        </li>
        <li>
            <a href="adminReports.jsp">Lista wszystkich raportów</a>
        </li>
        <li>
            <a href="usersReports.jsp">Lista raportów użytkownika</a>
        </li>
        <li>
            <a href="logout.do">Wyloguj</a>
        </li>
    </ul>
</div>
</body>
</html>

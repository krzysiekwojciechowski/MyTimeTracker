<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Timetracker!</title>
    <link rel="stylesheet" type="text/css" href="styles.css?version=1">
</head>
<body>

<div class="FPcontent">
    <h1>Rejestracja nowego użytkownika</h1>

    <form action="register.do" method="post">

        Login:<br/>
        <input type="text" name="userLogin">
        <br/>
        Hasło:<br/>
        <input type="password" name="password1">
        <br/>
        Powtórz hasło:<br/>
        <input type="password" name="password2">
        <br/>
        Imię:<br/>
        <input type="text" name="firstName">
        <br/>
        Nazwisko:<br/>
        <input type="text" name="secName">
        <br/>
        Rok urodzenia:<br/>
        <input type="number" name="birthYear">
        <br/>
        <br/>
        <input type="submit" value="Zarejestruj">
    </form>
</div>

<%@include file="header.jsp" %>
<%@include file="footer.jsp" %>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Time Tracker</title>
    <link rel="stylesheet" type="text/css" href="styles.css?version=1">
</head>
<body class="body">
<div class="content" style="text-align: center;">
    W razie problemow pisz pod adres: </br>
    klopoty@timetracker.kw
</div>

<%@include file="header.jsp" %>
<%@include file="menu.jsp" %>
<%@include file="footer.jsp" %>
</body>
</html>
